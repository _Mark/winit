#!/bin/sh
#   mutter-winit - Wayland protocol initializer
#   Copyright (C) 2021  Mark Williams, Jr.

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ -z "$1" ] || [ "$2" ]
then
    echo 'usage: mutter-winit <client>'
elif ! command -v $1
then
    echo 'error: client not found'
else
    exec mutter --sm-disable --wayland 2> >(
        head | grep X11 | cut -d : -f 6 | cut -d , -f 1 | {
            export DISPLAY=":`cat`"

            while ! $1
            do
                sleep .25
            done

            kill $$
        }
    )
fi

false
